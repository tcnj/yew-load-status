use std::convert::TryFrom;

use anyhow::Result;
use yew::{
    format::{Json, Nothing, Text},
    services::{
        fetch::{Request, Response, Uri},
        FetchService,
    },
    Callback,
};

use crate::NetTask;

pub fn perform_get<U, B, R>(url: U, body: B, callback: Callback<Result<R>>) -> NetTask
where
    Uri: TryFrom<U>,
    <Uri as TryFrom<U>>::Error: Into<http::Error>,
    B: Into<Text> + HasContentType,
    Json<Result<R>>: From<Text>,
    R: 'static,
{
    NetTask::new(callback, |callback| {
        let request = Request::get(url);

        let request = if let Some(content_type) = <B as HasContentType>::get_content_type() {
            request.header("Content-Type", content_type)
        } else {
            request
        };

        let request = request.body(body)?;

        let callback = callback.reform(|response: Response<Json<Result<R>>>| {
            let Json(result) = response.into_body();
            result
        });

        FetchService::fetch(request, callback)
    })
}

pub fn perform_post<U, B, R>(url: U, body: B, callback: Callback<Result<R>>) -> NetTask
where
    Uri: TryFrom<U>,
    <Uri as TryFrom<U>>::Error: Into<http::Error>,
    B: Into<Text> + HasContentType,
    Json<Result<R>>: From<Text>,
    R: 'static,
{
    NetTask::new(callback, |callback| {
        let request = Request::post(url);

        let request = if let Some(content_type) = <B as HasContentType>::get_content_type() {
            request.header("Content-Type", content_type)
        } else {
            request
        };

        let request = request.body(body)?;

        let callback = callback.reform(|response: Response<Json<Result<R>>>| {
            let Json(result) = response.into_body();
            result
        });

        FetchService::fetch(request, callback)
    })
}

pub trait HasContentType {
    fn get_content_type() -> Option<&'static str>;
}

impl HasContentType for Nothing {
    fn get_content_type() -> Option<&'static str> {
        None
    }
}

impl<T> HasContentType for Json<T> {
    fn get_content_type() -> Option<&'static str> {
        Some("application/json")
    }
}
