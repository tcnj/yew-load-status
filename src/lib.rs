use std::fmt;

use anyhow::{Error, Result};
use yew::{services::fetch::FetchTask, Callback};

pub mod fetch;

/// A generic type that represents the loading status of some data (`Ok`).
///
/// Possible values:
///
/// * `Loading(None)`: the data is currently loading and there
///   is no previous data to display in the meantime
/// * `Loading(Some(data))`: the data is currently loading, but
///   `data` can be displayed in the meantime, perhaps behind
///   a loading indicator
/// * `LoadErr(err)`: the error `err` was encountered whilst trying
///   to load the data
/// * `LoadOk(data)`: `data` was successfully loaded
#[derive(Debug)]
pub enum LoadStatus<Ok> {
    Loading(Option<Ok>, Option<NetTask>),
    LoadErr(Error),
    LoadOk(Ok),
}

#[must_use = "the request will be cancelled when the task is dropped"]
pub enum NetTask {
    None,
    Fetch(FetchTask),
}

impl NetTask {
    pub fn new<T, B, R>(callback: Callback<Result<T>>, build: B) -> NetTask
    where
        B: FnOnce(Callback<Result<T>>) -> Result<R>,
        R: Into<NetTask>,
    {
        let result = build(callback.clone());

        match result {
            Ok(t) => t.into(),
            Err(e) => {
                callback.emit(Err(e));
                NetTask::None
            }
        }
    }
}

impl From<FetchTask> for NetTask {
    fn from(t: FetchTask) -> NetTask {
        NetTask::Fetch(t)
    }
}

impl fmt::Debug for NetTask {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "<NetTask>")
    }
}

/// Implement the conversion of a `Result` into a `LoadStatus`
impl<Ok, Err> From<Result<Ok, Err>> for LoadStatus<Ok>
where
    Err: Into<Error>,
{
    fn from(result: Result<Ok, Err>) -> Self {
        match result {
            Ok(ok) => LoadStatus::LoadOk(ok),
            Err(err) => LoadStatus::LoadErr(err.into()),
        }
    }
}

impl<Ok> LoadStatus<Ok> {
    /// Use the function `f` to modify the data possibly held by this `LoadStatus`
    pub fn map<NewOk>(self, f: impl FnOnce(Ok) -> NewOk) -> LoadStatus<NewOk> {
        use LoadStatus::*;
        match self {
            Loading(loading, task) => Loading(loading.map(f), task),
            LoadOk(ok) => LoadOk(f(ok)),
            LoadErr(err) => LoadErr(err),
        }
    }

    /// Update this `LoadStatus` with any value that can be converted into the new `LoadStatus`
    pub fn update(&mut self, result: impl Into<LoadStatus<Ok>>) -> &mut Self {
        *self = result.into();
        self
    }

    /// Switch this `LoadStatus` into the `Loading` state, retaining any already loaded data
    pub fn reload(&mut self, task: Option<NetTask>) -> &mut Self {
        match *self {
            LoadStatus::Loading(_, ref mut t) => {
                *t = task;
            }
            LoadStatus::LoadErr(_) => *self = LoadStatus::Loading(None, task),
            LoadStatus::LoadOk(_) => {
                if let LoadStatus::LoadOk(r) =
                    std::mem::replace(self, LoadStatus::Loading(None, None))
                {
                    *self = LoadStatus::Loading(Some(r), task)
                }
            }
        }

        self
    }

    /// Get whether this `LoadStatus` is `Loading`
    pub fn is_loading(&self) -> bool {
        match self {
            LoadStatus::Loading(_, _) => true,
            _ => false,
        }
    }

    /// Get a reference to any loaded data stored
    pub fn current_results(&self) -> Option<&Ok> {
        match self {
            LoadStatus::Loading(o, _) => o.as_ref(),
            LoadStatus::LoadErr(_) => None,
            LoadStatus::LoadOk(r) => Some(r),
        }
    }
}

/// Implement a default value for the `LoadStatus` type
impl<Ok> Default for LoadStatus<Ok> {
    fn default() -> Self {
        LoadStatus::Loading(None, None)
    }
}

impl<Ok> From<NetTask> for LoadStatus<Ok> {
    fn from(task: NetTask) -> Self {
        LoadStatus::Loading(None, Some(task))
    }
}

impl<Ok> From<(Ok, NetTask)> for LoadStatus<Ok> {
    fn from((ok, task): (Ok, NetTask)) -> Self {
        LoadStatus::Loading(Some(ok), Some(task))
    }
}
